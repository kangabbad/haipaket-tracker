import React, { useState } from 'react';
import { Tab } from '@headlessui/react';
import Image from "next/image";

const Navbar = () => {
  const [activeTab, setActiveTab] = useState('profile');
  const handleChangeTab = (value: string) => {
    setActiveTab(value);
  }
  return (
    <div>
      <div className="bg-white w-full border-b border-gray-200 dark:border-gray-700">
        <ul className="flex justify-center w-full -mb-px text-sm font-medium text-center" id="myTab" data-tabs-toggle="#myTabContent" role="tablist">
          <li className="font-sans font-bold w-1/2 mr-2" role="presentation">
            <button
              className={`text-green-700 font-sans text-lg inline-block w-full p-4 border-b-2 rounded-t-lg ${activeTab === 'profile' ? 'border-green-700' : 'border-gray-200 hover:border-gray-300 dark:border-gray-700'}`}
              id="profile-tab"
              data-tabs-target="#profile"
              type="button"
              role="tab"
              onClick={() => handleChangeTab('profile')}
            >
              Penjual
            </button>
          </li>
          <li className="font-sans font-bold w-1/2 mr-2" role="presentation">
            <button
              className={`text-green-700 font-sans text-lg inline-block w-full p-4 border-b-2 rounded-t-lg ${activeTab === 'dashboard' ? 'border-green-700' : 'border-gray-200 hover:border-gray-300 dark:border-gray-700 '}`}
              id="dashboard-tab"
              data-tabs-target="#dashboard"
              type="button"
              role="tab"
              onClick={() => handleChangeTab('dashboard')}
            >
              Pembeli
            </button>
          </li>
        </ul>
      </div>
    </div>
  );
}

const InputTrackingNumber = () => {
  return (
    <div id="myTabContent">
    <div className="bg-white dark:bg-white" id="profile" role="tabpanel" aria-labelledby="profile-tab">
      <div className="bg-gray-100 flex items-center justify-center">
        <div className="max-w-d mx-auto bg-white">
          <div className="mb-6">
            <Image
              src={require("@/assets/images/Home_1.webp")}
              alt="gambar haipaket"
            />
          </div>
        </div>
      </div>
      <div className="font-bold text-2xl font-sans text-slate-600 text-center bg-white">
        <p>Kurir Rekomendasi,<br />
          Pengiriman Canggih dan<br />Praktis</p>
      </div>
      <form>
        <div className="flex bg-white items-center justify-center mb-4 mt-4">
          <input type="text" id="name" placeholder="Masukkan Nomor Resi (Misalkan : TKP01-XXXX-XXXX)" className="placeholder:text-sm w-80 p-2 border border-gray-300 rounded-md"
          />
        </div>
        <div className="flex items-center justify-center mb-20">
        <button type="submit" className="w-80 font-bold bg-green-500 text-white py-2 px-4 rounded-md">
          Lacak Paket
        </button>
        </div>
      </form>
    </div>
  </div>
  )
}

const Information = () => {
  return null
}

const Partner = () => {
  return null
}

const Benefits = () => {
  return null
}

const Homepage = () => {
  return (
    <div>
      <Navbar />
      <InputTrackingNumber />
      <Information />
      <Partner />
      <Benefits />
    </div>
  )
}

export default Homepage
